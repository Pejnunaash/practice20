// Fill out your copyright notice in the Description page of Project Settings.


#include "Debuff.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ADebuff::ADebuff()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DebuffComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DebuffComponent"));
	RootComponent = DebuffComponent;
}

// Called when the game starts or when spawned
void ADebuff::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADebuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADebuff::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->DecreaseMovementSpeed();
			this->Destroy();
		}
	}
}

