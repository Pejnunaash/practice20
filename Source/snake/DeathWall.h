// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "Interactable.h"
#include "DeathWall.generated.h"

class ASnakeBase;
class APlayerPawnBase;

UCLASS()
class SNAKE_API ADeathWall : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADeathWall();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* WallMeshComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UBoxComponent* WallBoxComponent;

	UPROPERTY()
		ASnakeBase* SnakeOwner;

	UPROPERTY()
		APlayerPawnBase* PlayerPawnOwner;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
};
