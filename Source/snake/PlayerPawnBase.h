// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Food.h"
#include "Bonus.h"
#include "Debuff.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.generated.h"


class UCameraComponent;
class ASnakeBase;
class AFood;
class ABonus;
class ADebuff;



UCLASS()
class SNAKE_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
	//Food
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FoodSpawnSettings)
		float StepDelay = 2.f;
	//Food
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = FoodSpawnSettings)
		float BuferTime = 0;

	//bonus
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BonusSpawnSettings)
		float BStepDelay = 30.f;
	//bonus
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BonusSpawnSettings)
		float BBuferTime = 0;

	//debuff
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DebuffSpawnSettings)
		float DStepDelay = 120.f;
	//debuff
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DebuffSpawnSettings)
		float DBuferTime = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameMode)
		bool IsDead = false;

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(BlueprintReadWrite)
		ABonus* BonusActor;

	UPROPERTY(BlueprintReadWrite)
		ADebuff* DebuffActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonus> BonusActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ADebuff> DebuffActorClass;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, Category="GameMode")
	void CreateSnakeActor();

	void CreateFoodActor();

	void CreateBonusActor();

	void CreateDebuffActor();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
	


	int32 GameMode = 0;

	UFUNCTION(BlueprintCallable, Category="GameMode")
		int32 GetGameMode() const { return GameMode; }

	UFUNCTION(BlueprintCallable, Category = "GameMode")
		int32 GetScore();

	UFUNCTION(BlueprintCallable, Category = "GameMode")
		bool Dead();
};
